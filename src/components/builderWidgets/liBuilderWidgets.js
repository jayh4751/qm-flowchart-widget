// WIDGETS
const widgets = [
  // TEXT
  {
    name: "Text",
    type: "Text",
    icon: "textarea-t",
    defaultWidth: 12,
    defaultHeight: 3,
    defaultValue: "",
    links: [false],
  },
  // BUTTONS
  {
    name: "Button(s)",
    type: "Buttons",
    icon: "menu-button",
    defaultWidth: 6,
    defaultHeight: 6,
    defaultValue: [],
    links: [true, "widgetValue"],
  },
  // DIAGRAM
  {
    name: "Diagram [alpha]",
    type: "Diagram",
    icon: "diagram3",
    defaultWidth: 14,
    defaultHeight: 6,
    defaultValue: {
      links: [],
      nodes: [],
    },
    links: [true, "widgetValue.nodes"],
  },
  // IMAGE
  {
    name: "Image",
    type: "File",
    icon: "image",
    defaultWidth: 8,
    defaultHeight: 9,
    defaultValue: "",
    links: [false],
  },
  // HOTSPOTS
  {
    name: "Image with hotspots [beta]",
    type: "Hotspots",
    icon: "image-alt",
    defaultWidth: 6,
    defaultHeight: 6,
    defaultValue: [],
    links: [true, "widgetValue"],
  },
  // DOCUMENT
  {
    name: "Document download",
    type: "Document",
    icon: "file-earmark-richtext",
    defaultWidth: 2,
    defaultHeight: 4,
    defaultValue: {
      path: "",
      type: "",
      filename: "",
    },
    links: [false],
  },
  // VIDEO
  {
    name: "Youtube video embed",
    type: "Video",
    icon: "youtube",
    defaultWidth: 12,
    defaultHeight: 9,
    defaultValue: "",
    links: [false],
  },
  // IFRAME
  {
    name: "iFrame",
    type: "iFrame",
    icon: "layout-text-window-reverse",
    defaultWidth: 12,
    defaultHeight: 6,
    defaultValue: "",
    links: [false],
  },
  // FORM
  {
    name: "Form",
    type: "Form",
    icon: "view-stacked",
    defaultWidth: 4,
    defaultHeight: 8,
    defaultValue: [],
    links: [false],
  },
  // TABLE
  {
    name: "Table",
    type: "Table",
    icon: "table",
    defaultWidth: 12,
    defaultHeight: 8,
    defaultValue: {
      headers: [],
      items: [],
    },
    links: [false],
  },
  // OBJECT
  {
    name: "Object",
    type: "Object",
    icon: "card-list",
    defaultWidth: 12,
    defaultHeight: 8,
    defaultValue: {
      entity: {
        name: "",
      },
      headers: [
        {
          label: "Name",
          id: "_li_name",
        },
        {
          label: "Type",
          id: "_li_type",
        },
        {
          label: "Mandatory",
          id: "_li_mandatory",
        },
        {
          label: "Default value",
          id: "_li_defaultvalue",
        },
        {
          label: "Foreign key",
          id: "_li_foreignkey",
        },
        {
          label: "Comments",
          id: "_li_comments",
        },
      ],
      items: [
        {
          _li_name: "",
          _li_type: "singleline",
          _li_mandatory: "1",
          _li_defaultvalue: "",
          _li_foreignkey: "",
          _li_comments: "",
        },
      ],
    },
    links: [false],
  },
  // DYNMIC OBJECT
  // {
  //   name: "Dynamic Object [experimental]",
  //   type: "ObjectDynamic",
  //   icon: "card-list",
  //   defaultWidth: 12,
  //   defaultHeight: 8,
  //   defaultValue: {
  //     entity:{
  //       "name": '',
  //     },
  //     headers: [{
  //       "label": "Name",
  //       "id": "_li_name"
  //     }, {
  //       "label": "Type",
  //       "id": "_li_type"
  //     }, {
  //       "label": "Mandatory",
  //       "id": "_li_mandatory"
  //     }, {
  //       "label": "Default value",
  //       "id": "_li_defaultvalue"
  //     }, {
  //       "label": "Foreign key",
  //       "id": "_li_foreignkey"
  //     }, {
  //       "label": "Comments",
  //       "id": "_li_comments"
  //     }],
  //     items: [{
  //       "_li_name": "",
  //       "_li_type": "singleline",
  //       "_li_mandatory": "1",
  //       "_li_defaultvalue": "",
  //       "_li_foreignkey": "",
  //       "_li_comments": ""
  //     }],
  //   }
  // },
];
export default widgets;
