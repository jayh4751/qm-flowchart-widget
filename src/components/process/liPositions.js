/**
 * @param element {HTMLElement}
 * @return {{top: number, left: number}}
 */
function getOffsetRect(element) {
    let box = element.getBoundingClientRect()

    let scrollTop = window.pageYOffset
    let scrollLeft = window.pageXOffset

    let top = box.top + scrollTop
    let left = box.left + scrollLeft

    return {
        top: Math.round(top),
        left: Math.round(left)
    }
}
/**
 * @param event {MouseEvent}
 * @param element {HTMLElement}
 * @return {{x: number, y: number}}
 */
function getMousePosition(element, event) {
    let touchstart = event.type === "touchstart" || event.type === "touchmove",
        mouseX, 
        mouseY;
    if (touchstart == true){
        mouseX = event.targetTouches[0].pageX;
        mouseY = event.targetTouches[0].pageY;
    } else {
        mouseX = event.pageX || event.clientX + document.documentElement.scrollLeft;
        mouseY = event.pageY || event.clientY + document.documentElement.scrollTop;
    }
    

    let offset = getOffsetRect(element)
    let x = mouseX - offset.left
    let y = mouseY - offset.top
    //  console.log(x + ' ' + y)
    return [x, y];
}

export {
    getMousePosition,
    getOffsetRect
}