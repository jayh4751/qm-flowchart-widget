import axios from 'axios';
const API_URL = process.env.VUE_APP_API_URL;
class DatabaseService {
    /**
     *  getAll [allowed] database tables
     */
    getAll() {
        const config = {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': 'Bearer ' + localStorage.getItem('liSession')
            }
        }
        return axios
            .get(API_URL + 'api/lidatabase/getall', config)
            .then(function (response) {
                return Promise.resolve(response.data);
            }).catch(error => {
                return Promise.reject(error);
            });
    }
    updateField(object) {
        const config = {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem('liSession')
            }
        }
        return axios
            .post(API_URL + 'api/lidatabase/updatefield', object, config)
            .then(function (response) {
                return Promise.resolve(response.data);
            }).catch(error => {
                return Promise.reject(error);
            });
    }


}
export default new DatabaseService();