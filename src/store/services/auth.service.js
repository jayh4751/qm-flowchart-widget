import axios from 'axios';
const API_URL = process.env.VUE_APP_API_URL;
// const API_CLIENT = process.env.VUE_APP_API_CLIENT;
// const API_SECRET = process.env.VUE_APP_API_SECRET;
// const TOKEN = Buffer.from(`${API_CLIENT}:${API_SECRET}`, 'utf8').toString('base64');

const config = {
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
    }
}

class AuthService {
    login(user) {
        let obj = {
            "email": user.username,
            "password": user.password
        }
        return axios
            .post(`${API_URL}?-a=loginConnect`, obj, config)
            .then(response => {
                if (response.data.status === 0 && response.data.guid){
                    localStorage.setItem('liSession', response.data.guid);
                    return Promise.resolve(response.data);
                } else {
                    return Promise.reject(response.data);
                }
            }, error => {
                return Promise.error({
                    status: 'error',
                    error: error
                });
            });
    }
    logout() {
        let obj = {
            "guid": localStorage.getItem('liSession')
        }
        return axios
            .post(`${API_URL}?-a=disconnect`, obj, config)
            .then(response => {
                localStorage.removeItem('liSession');
                return Promise.resolve(response);
            });
           
    }
    reconnect() {
        let obj = {
            "guid": localStorage.getItem('liSession')
        }
       return axios
           .post(`${API_URL}?-a=reconnectUser`, obj, config)
            .then(response => {
                if (response.data.guid) {
                    localStorage.setItem('liSession', response.data.guid);
                    return Promise.resolve(response.data);
                } else {
                    localStorage.removeItem('liSession');
                    return Promise.resolve({
                        status: 'error',
                        error: 'error'
                    });
                }
            }, error => {
                console.log(error)
                localStorage.removeItem('liSession');
                return Promise.reject({
                    status: 'error',
                    error: error
                });
            });
    }
    forgotPassword(email){
        let obj = {
            "email": email
        }
        return axios
            .post(`${API_URL}?-a=forgotPassword`, obj, config)
            .then(response => {
                return Promise.resolve(response);
            });
    }
}
export default new AuthService();