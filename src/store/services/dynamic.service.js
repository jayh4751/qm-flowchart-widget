import axios from 'axios';
const API_URL = process.env.VUE_APP_API_URL;
const HEADERS = {
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
    }
}
class DynamicService {
    getCollection(object) {
        object.guid = localStorage.getItem('liSession');
        return axios
            .post(API_URL + '?-a=query', object, HEADERS)
            .then(function (response) {
                return Promise.resolve(response.data);
            }).catch(error => {
                return Promise.reject(error);
            });
    }
    getCollectionItem(object) {
        object.guid = localStorage.getItem('liSession');
        return axios
            .post(API_URL + '?-a=loadByPK', object, HEADERS)
            .then(function (response) {
                return Promise.resolve(response.data);
            }).catch(error => {
                return Promise.reject(error);
            });
    }
    updateCollectionItem(object) {
        object.guid = localStorage.getItem('liSession');
        return axios
            .post(API_URL + '?-a=updateByPK', object, HEADERS)
            .then(function (response) {
                return Promise.resolve(response.data);
            }).catch(error => {
                return Promise.reject(error);
            });
    }
    createCollectionItem(object) {
        object.guid = localStorage.getItem('liSession');
        return axios
            .post(API_URL + '?-a=insert', object, HEADERS)
            .then(function (response) {
                return Promise.resolve(response.data);
            }).catch(error => {
                return Promise.reject(error);
            });
    }
    setOutputNodeSequence(object) {
        object.guid = localStorage.getItem('liSession');
        return axios
            .post(API_URL + '?-a=bulkUpdateByPK', object, HEADERS)
            .then(function (response) {
                return Promise.resolve(response.data);
            }).catch(error => {
                return Promise.reject(error);
            });
    }
    removeCollectionItem(object) {
        object.guid = localStorage.getItem('liSession');
        return axios
            .post(API_URL + 'api/collection/removeCollectionItem', object, HEADERS)
            .then(function (response) {
                return Promise.resolve(response.data);
            }).catch(error => {
                return Promise.reject(error);
            });
    }
    moveCollectionItem(object) {
        object.guid = localStorage.getItem('liSession');
        return axios
            .post(API_URL + '?-a=move', object, HEADERS)
            .then(function (response) {
                return Promise.resolve(response.data);
            }).catch(error => {
                return Promise.reject(error);
            });
    }
    clonePresentation(object) {
        object.guid = localStorage.getItem('liSession');
        return axios
            .post(API_URL + 'api/collection/clonepresentation', object, HEADERS)
            .then(function (response) {
                return Promise.resolve(response.data);
            }).catch(error => {
                return Promise.reject(error);
            });
    }
    movePresentation(object){
        object.guid = localStorage.getItem('liSession');
        return axios
            .post(API_URL + 'api/collection/movepresentation', object, HEADERS)
            .then(function (response) {
                return Promise.resolve(response.data);
            }).catch(error => {
                return Promise.reject(error);
            });
    }
    getAllPresentations(object) {
        object.guid = localStorage.getItem('liSession');
        return axios
            .post(API_URL + '?-a=getFullProject', object, HEADERS)
            .then(function (response) {
                return Promise.resolve(response.data);
            }).catch(error => {
                return Promise.reject(error);
            });
    }

}
export default new DynamicService();