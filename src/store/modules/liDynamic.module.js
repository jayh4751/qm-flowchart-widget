import DynamicService from "../services/dynamic.service";
export const liDynamic = {
  namespaced: true,
  state: {
    dynamicContent: {},
  },
  actions: {
    GET_COLLECTION({ commit }, _data) {
      const _vm = _data.vm ? _data.vm : "";
      const _additionalName = _data.additionalName ? _data.additionalName : "";
      const obj = {
        entity: _data.object,
        where: _data.where ? _data.where : "",
        orderBy: _data.orderBy ? _data.orderBy : "",
      };
      obj.where = `${obj.where}& deleted_at=#null`;

      return DynamicService.getCollection(obj).then(
        (response) => {
          response.additionalName = _additionalName;
          commit("setCollection", response);
          return Promise.resolve(response.data);
        },
        (error) => {
          _vm.$bvToast.toast(error.statusText, {
            title: "Error",
            variant: "danger",
            autoHideDelay: 300,
            toaster: "b-toaster-bottom-right",
          });
        }
      );
    },
    GET_COLLECTION_ITEM({ commit }, _data) {
      const _vm = _data.vm ? _data.vm : "";
      const obj = {
        entity: _data.object ? _data.object : "",
        pk: _data.id ? _data.id : "",
        // where: _data.where ? _data.where : "",
        resolveFK: true
      };
      // obj.where = `${obj.where}& deleted_at=#null`;
      if (obj.object != "" && _vm != "") {
        return DynamicService.getCollectionItem(obj).then(
          (response) => {
            if (_data.options && _data.options.update == "store") {
              // console.log(_data.object,response);
              const obj = {
                additionalName: _data.additionalName
                  ? _data.additionalName
                  : _data.object,
                payload: response.data,
              };
              commit("setCollectionItem", obj);
            } else {
              commit("emptyFn");
            }
            return Promise.resolve(response.data);
          },
          (error) => {
            _vm.$bvToast.toast(error.message, {
              title: "Error",
              variant: "danger",
              autoHideDelay: 300,
              toaster: "b-toaster-bottom-right",
            });
          }
        );
      }
    },
    CREATE_COLLECTION_ITEM({ commit }, _data) {
      let _vm = _data.vm ? _data.vm : "";
      let _options = _data.options ? _data.options : "";
      const obj = {
        entity: _data.object ? _data.object : "",
        properties: _data.item ? _data.item : "",
        reset: true,
        setAutomatics: true,
        lenient: true
      };
      if (obj.object != "" && _vm != "") {
        // Set created_by field
        obj.properties.created_by = _vm.app_user.name;
        obj.properties.created_at =  new Date().toISOString();
        return DynamicService.createCollectionItem(obj).then(
          (response) => {
            if (!_options.silent) {
              _vm.$bvToast.toast("The item is created", {
                title: "Success",
                autoHideDelay: 300,
                toaster: "b-toaster-bottom-right",
              });
            }
            if (_options.update === "store") {
              const commitData = {
                _vm: _vm,
                item: response.bo,
                object: _data.object,
                additionalName: _options.additionalName,
              };
            //  console.log('test');
              commit("insertCollectionItem", commitData);
              return Promise.resolve(response.bo);
            } else {
              commit("emptyFn");
              return Promise.resolve(response.bo);
            }
          },
          (error) => {
            _vm.$bvToast.toast(error.message, {
              title: "Error",
              variant: "danger",
              autoHideDelay: 500,
            });
          }
        );
      }
    },
    UPDATE_COLLECTION_ITEM({ commit }, _data) {
      let _vm = _data.vm ? _data.vm : "";
      let _options = _data.options ? _data.options : "";
      let _addUpdateFields = _options.addUpdateFields && _options.addUpdateFields == 'no' ? false : true ;
      const obj = {
        entity: _data.item.entity,
        PK: _data.item.properties.identifier,
        properties: _data.item.properties,
      };

      if (_options.removeKey){
        delete obj.properties[_options.removeKey];
      }
      
      _vm.removeKeyStartsWith(obj.properties, "_");
    
      if (obj.entity != "" && _vm != "") {
        if(_addUpdateFields){
          obj.properties.updated_by = _vm.app_user.name;
          obj.properties.updated_at =  new Date().toISOString();
        }
        

        return DynamicService.updateCollectionItem(obj).then(
          (response) => {
            if (!_options.silent) {
              _vm.$bvToast.toast("The item is updated", {
                title: "Success",
                autoHideDelay: 300,
                toaster: "b-toaster-bottom-right",
              });
            }

            commit("emptyFn");
            return Promise.resolve(response.payload);
          },
          (error) => {
            _vm.$bvToast.toast(error.message, {
              title: "Error",
              variant: "danger",
              autoHideDelay: 500,
            });
          }
        );
      }
    },
    REMOVE_COLLECTION_ITEM({ commit }, _data) {
      let _vm = _data.vm ? _data.vm : "";
      let _options = _data.options ? _data.options : "";
      // console.log(_data);
      const obj = {
        entity: _data.item.entity,
        PK: _data.item.properties.identifier,
        properties: _data.item.properties,
      };
      _vm.removeKeyStartsWith(obj.properties, "_");
    
      if (obj.entity != "" && _vm != "") {
          // Set deleted_by field
        obj.properties.deleted_by = _vm.app_user.name;
        obj.properties.deleted_at = new Date().toISOString();
        

        const h = _vm.$createElement;
        const messageDelete = h("div", {
          domProps: {
            innerHTML:
              `<h3>Your're about to <b>delete</b> a ${_options.itemName.toLowerCase()}</h3><i> Are you sure? This cannot be undone!</i>`,
          },
        });
        const deleteItem_service = function(obj, _options) {
          // Delete the object via promise so we can send it back to the component
          // return promise
          return DynamicService.updateCollectionItem(obj).then(
            (response) => {
              if (_options.update === "store") {
                const commitData = {
                  _vm: _vm,
                  item: obj.properties,
                  additionalName: _options.additionalName,
                };
                commit("removeCollectionItem", commitData);
                return Promise.resolve(response.payload);
              } else {
                commit("emptyFn");
                return Promise.resolve(response.payload);
              }
            },
            (error) => {
              _vm.$bvToast.toast(error.message, {
                title: "Error",
                variant: "danger",
                autoHideDelay: 500,
              });
            }
          );
        };
        if (!_options.silent) {
          return _vm.$bvModal
            .msgBoxConfirm([messageDelete], {
              title: "Please confirm",
              headerBgVariant: "danger",
              headerTextVariant: "light",
              size: "md",
              okTitle: "Delete",
              buttonSize: "md",
              okVariant: "danger",
              cancelVariant: "outline-dark",
              cancelClass: "px-3",
              headerClass: "p-2 border-bottom-0",
              bodyClass: "text-center p-5",
              footerClass:
                "p-2 border-top-0 apcModalFooter justify-content-center",
            })
            .then((value) => {
              if (value) {
                return deleteItem_service(obj, _options);
              } else {
                return false;
              }
            });
        } else {
          return deleteItem_service(obj, _options);
        }
      }
    },
    MOVE_COLLECTION_ITEM({ commit }, _data) {
      let _vm = _data.vm ? _data.vm : "";
      const obj = {
        entity: _data.object,
        pk: _data.item.properties.identifier,
        after: _data.after,
      };

      if (obj.object != "" && _vm != "") {
        return DynamicService.moveCollectionItem(obj).then(
          (response) => {
            _vm.$bvToast.toast("The positions are updated", {
              title: "Success",
              autoHideDelay: 300,
              toaster: "b-toaster-bottom-right",
            });

            commit("emptyFn");
            return Promise.resolve(response.payload);
          },
          (error) => {
            _vm.$bvToast.toast(error.message, {
              title: "Error",
              variant: "danger",
              autoHideDelay: 500,
            });
          }
        );
      }
    },
    SET_OUTPUTNODE_SEQUENCE({ commit }, _data) {
      let _vm = _data.vm ? _data.vm : "";
      const obj = {
        entity: _data.object ? _data.object : "",
        properties: _data.item ? _data.item : "",
      };
      if (obj.entity != "" && _vm != "") {
        return DynamicService.setOutputNodeSequence(obj).then(
          (response) => {
            _vm.$bvToast.toast("The sequence is saved", {
              title: "Success",
              autoHideDelay: 300,
              toaster: "b-toaster-bottom-right",
            });
            commit("emptyFn");
            return Promise.resolve(response.payload);
          },
          (error) => {
            _vm.$bvToast.toast(error.message, {
              title: "Error",
              variant: "danger",
              autoHideDelay: 500,
            });
          }
        );
      }
    },
    CLONE_PRESENTATION({ commit }, _data) {
      let _vm = _data.vm ? _data.vm : "";
      const obj = {
        id: _data.id ? _data.id : "",
        identifier: _data.identifier ? _data.identifier : "",
        newIdentifier: _vm.generateUUID(),
        user: _data.user,
      };
      if (
        obj.identifier != "" &&
        obj.id != "" &&
        obj.newIdentifier != "" &&
        _vm != ""
      ) {
        return DynamicService.clonePresentation(obj).then(
          (response) => {
            commit("emptyFn");
            return Promise.resolve(response.payload);
          },
          (error) => {
            _vm.$bvToast.toast(error.message, {
              title: "Error",
              variant: "danger",
              autoHideDelay: 500,
            });
          }
        );
      }
    },
    MOVE_PRESENTATION({ commit }, _data) {
      let _vm = _data.vm ? _data.vm : "";
      const obj = {
        item: _data.item ? _data.item : "",
        currentProject: _data.currentProject ? _data.currentProject : "",
        user: _data.user,
      };
      if (obj.item != "" && obj.currentProject != "" && _vm != "") {
        return DynamicService.movePresentation(obj).then(
          (response) => {
            commit("emptyFn");
            return Promise.resolve(response.payload);
          },
          (error) => {
            _vm.$bvToast.toast(error.message, {
              title: "Error",
              variant: "danger",
              autoHideDelay: 500,
            });
          }
        );
      }
    },
    GET_ALL_PRESENTATIONS({ commit }, _data) {
      let _vm = _data.vm ? _data.vm : "";
      const obj = {
        project: _data.identifier ? _data.identifier : ""
      };
      if (obj.object != "" && _vm != "") {
        return DynamicService.getAllPresentations(obj).then(
          (response) => {
            commit("emptyFn");
            // console.log('all ? ',response.data);
            return Promise.resolve(response.data);
          },
          (error) => {
            _vm.$bvToast.toast(error.message, {
              title: "Error",
              variant: "danger",
              autoHideDelay: 500,
            });
          }
        );
      }
    },
  },
  mutations: {
    emptyFn: () => {
      return true;
    },
    setCollection(state, data) {
      let name = data.additionalName != "" ? data.additionalName : data.object;
      state.dynamicContent = {
        ...state.dynamicContent,
        [name]: data.data,
      };
    },
    setCollectionItem(state, data) {
      let name = data.additionalName != "" ? data.additionalName : data.object;
      state.dynamicContent = {
        ...state.dynamicContent,
        [name]: data.payload,
      };
    },
    insertCollectionItem: (state, data) => {
      let name = data.additionalName != "" ? data.additionalName : data.object;
      data.item = Array.isArray(data.item) ? data.item[0] : data.item;
      state.dynamicContent[name] = [data.item, ...state.dynamicContent[name]];
    },
    removeCollectionItem: (state, data) => {
      let name = data.additionalName != "" ? data.additionalName : data.object;
      state.dynamicContent[name].splice(
        state.dynamicContent[name].findIndex(function(i) {
          return i.properties.identifier === data.item.identifier;
        }),
        1
      );
    },
  },
  getters: {
    getCollection: (state) => (name) => {
      if (state.dynamicContent[name] != undefined) {
        return state.dynamicContent[name];
      } else {
        return "error";
      }
    },
    getAccount: (state) =>  {
      if (state.dynamicContent['account'] != undefined) {
        return state.dynamicContent['account'];
      } else {
        return null;
      }
    },
    getCollectionItem: (state) => (name, key, value) => {
      if (state.dynamicContent[name] != undefined) {
        return state.dynamicContent[name].find((item) => {
          return item[key] === value;
        });
      } else {
        return "error";
      }
    },
    getFilteredNotes: (state) => (identifier) =>{
      if (state.dynamicContent['presentationNotes'] != undefined) {
      return state.dynamicContent['presentationNotes'].filter((item) => {
        return item.properties.slide === identifier;
      });
    } else {
      return [];
    }
    }
  },
};
