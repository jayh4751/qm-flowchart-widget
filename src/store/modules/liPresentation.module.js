export const liPresentation = {
  namespaced: true,
  state: {
    liBreadCrumbs: {},
    custom: {},
  },
  actions: {
    ADD_BREADCRUMB({ commit }, _data) {
      commit("setBreadCrumb", _data);
    },
    SET_CUSTOM_VALUE({ commit }, obj) {
      commit("setCustomValue", obj);
    },
  },
  mutations: {
    setCustomValue(state, obj) {
      console.log(state, obj);
      state.custom[obj.path] = obj.value;
      // localStorage.setItem("appCourtCustom", JSON.stringify(state.custom));
    },
    setBreadCrumb(state, data) {
      let name = data.presentation.name;
      if (state.liBreadCrumbs[name] != undefined) {
        state.liBreadCrumbs[name] = [...state.liBreadCrumbs[name], data.slide];
      } else {
        state.liBreadCrumbs[name] = [data.slide];
      }
    },
  },
  getters: {
    getBreadCrumbs: (state) => {
      return state.liBreadCrumbs;
    },
  },
};
