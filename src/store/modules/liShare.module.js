import ShareService from "../services/share.service";
export const liShare = {
  namespaced: true,
  state: {
    config: {},
    loading: false,
    shares: {},
    presentations: {},
    liBreadCrumbs: {},
    liBreadCrumbLevel: 0,
    custom: {},
  },
  actions: {
    SET_CUSTOM_VALUE({ commit }, obj) {
      commit("setCustomValue", obj);
    },
    UPDATE_BRADCRUMBLEVEL({ commit }, _data) {
      commit("updateBreadCrumbLevel", _data);
    },
    ADD_BREADCRUMB({ commit }, _data) {
      commit("setBreadCrumb", _data);
    },
    GET_SHARE({ commit }, _data) {
      let _identifier = _data.identifier ? _data.identifier : "";
      let _name = _data.name ? _data.name : "";
      if (_identifier != "") {
        return ShareService.getShare(_identifier).then(
          (response) => {
            localStorage.setItem(
              "appCourtShare",
              JSON.stringify(response.guid)
            );
            commit("setShare", {
              name: _name,
              payload: response.data,
            });
            return Promise.resolve(response.data);
          },
          (error) => {
            console.log(error);
          }
        );
      }
    },
    GET_PRESENTATION({ commit }, _data) {
      const guid = JSON.parse(localStorage.getItem("appCourtShare"));
      let _payload = {
        guid: guid,
        entity: "appcourt/api_presentation",
        group: "*",
        pk: _data.identifier,
        resolveFK: true,
      };
      let _root = ShareService.getPresentation(_payload).then((response) => {
        return Promise.resolve(response.data);
      });
      return _root.then((val) => {
        commit("setPresentation", {
          name: _data.identifier,
          payload: val,
        });
        return Promise.resolve(val);
      });
    },
    GET_LINKS({ commit }, _data) {
      const guid = JSON.parse(localStorage.getItem("appCourtShare"));
      let _payloadLinks = {
        guid: guid,
        entity: "appcourt/api_presentation_link",
        group: "*",
        where: `foreign_identifier = '${_data}'`,
        resolveFK: true,
        orderBy: "-id",
      };
      const links = ShareService.query(_payloadLinks).then((response) => {
        return Promise.resolve(response.data);
      });
      return links.then((val) => {
        commit("setPresentationLinks", {
          name: _data,
          payload: val,
        });
      });
    },
    GET_NODES({ commit }, _data) {
      const guid = JSON.parse(localStorage.getItem("appCourtShare"));
      let _payloadNodes = {
        guid: guid,
        entity: "appcourt/api_presentation_node",
        group: "*",
        where: `foreign_identifier = '${_data}'`,
        resolveFK: true,
        orderBy: "-id",
      };
      const nodes = ShareService.query(_payloadNodes).then((response) => {
        return Promise.resolve(response.data);
      });
      return nodes.then((val) => {
        commit("setPresentationNodes", {
          name: _data,
          payload: val,
        });
      });
    },
    GET_PRESENTATION_WITH_INVITE({ commit }, _data) {
      let _identifier = _data.identifier ? _data.identifier : "";
      let _code = _data.code ? _data.code : "";
      let _name = _data.name ? _data.name : "";
      if (_identifier != "") {
        return ShareService.getPresentationWithInvite({
          identifier: _identifier,
          code: _code,
        }).then(
          (response) => {
            commit("setPresentation", {
              name: _name,
              payload: response.payload,
            });
            return Promise.resolve(response.payload);
          },
          (error) => {
            console.log(error);
          }
        );
      }
    },
    GET_NOTES({ commit }, _data) {
      const guid = JSON.parse(localStorage.getItem("appCourtShare"));
      let _obj = {
        guid: guid,
        entity: "appcourt/api_note",
        group: "*",
        where: `slide = '${_data.slideIdentifier}'`,
        resolveFK: true,
        orderBy: "id",
      };

      return ShareService.getNotes(_obj).then(
        (response) => {
          commit("emptyFN", {
            payload: response.payload,
          });
          return Promise.resolve(response.data);
        },
        (error) => {
          console.log(error);
        }
      );
    },
    CREATE_NOTE({ commit }, _data) {
      let _obj = {
        guid:JSON.parse(localStorage.getItem("appCourtShare")),
        entity: "appcourt/api_note",
        properties: _data.item ? _data.item : "",
        reset: true,
        setAutomatics: true,
        lenient: true
      }
      return ShareService.createNote(_obj).then(
        (response) => {
          commit("emptyFN", {
            payload: response.data,
          });
          return Promise.resolve(response.data);
        },
        (error) => {
          console.log(error);
        }
      );
    },
  },
  mutations: {
    //eslint-disable-next-line
    emptyFN: (state) => {},
    setCustomValue(state, obj) {
      console.log(state, obj);
      state.custom[obj.path] = obj.value;
      // localStorage.setItem("appCourtCustom", JSON.stringify(state.custom));
    },
    updateBreadCrumbLevel(state) {
      state.liBreadCrumbLevel += 1;
    },
    setBreadCrumb(state, data) {
      let name = data.presentation.name;
      if (
        state.liBreadCrumbs[state.liBreadCrumbLevel + " - " + name] != undefined
      ) {
        state.liBreadCrumbs[state.liBreadCrumbLevel + " - " + name] = [
          ...state.liBreadCrumbs[state.liBreadCrumbLevel + " - " + name],
          data.slide,
        ];
      } else {
        state.liBreadCrumbs[state.liBreadCrumbLevel + " - " + name] = [
          data.slide,
        ];
      }
    },
    setLoading: (state, status) => {
      state.loading = status;
    },
    setShare(state, data) {
      state.presentations = {
        ...state.presentations,
        [data.payload.properties._presentation.properties.identifier]:
          data.payload.properties._presentation,
      };
      delete data.payload.properties._presentation;

      state.shares = {
        ...state.shares,
        [data.name]: data.payload,
      };
    },
    setPresentation(state, data) {
      state.presentations = {
        ...state.presentations,
        [data.name]: data.payload,
      };
    },
    setPresentationNodes(state, data) {
      state.presentations[data.name].properties._allSlides = data.payload;
    },
    setPresentationLinks(state, data) {
      state.presentations[data.name].properties._allLinks = data.payload;
    },
  },
  getters: {
    getBreadCrumbs: (state) => {
      return state.liBreadCrumbs;
    },
    getLoading: (state) => {
      return state.loading;
    },
    getAppConfig: (state) => {
      return state.config;
    },
    getShare: (state) => (name) => {
      if (state.shares[name]) {
        return state.shares[name];
      } else {
        return null;
      }
    },
    getPresentation: (state) => (name) => {
      if (state.presentations[name]) {
        return state.presentations[name];
      } else {
        return null;
      }
    },
  },
};
