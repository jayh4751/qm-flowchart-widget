import StaticService from "../services/static.service";
export const liStatic = {
  namespaced: true,
  state: {
    config: {},
    staticContent: {},
    liloading: false,
    liBuilder: false,
    liPresenting: false,
    liBuilderSettingsItem: null,
    liModal: {
      headerTitle: "",
      headerBgVariant: "",
      headerTextVariant: "",
      bodyBgVariant: "",
      size: "",
      scrollable: true,
      noCloseOnEsc: false,
      noCloseOnBackdrop: false,
      hideHeaderClose: false,
      hideBackdrop: true,
    },
  },
  actions: {
    GET_FEATURE({ commit }, _data) {
      let _vm = _data.vm ? _data.vm : "";
      let _identifier = _data.identifier ? _data.identifier : "";
      // let _query = (_data.query) ? _data.query : '';
      let _additionalName = _data.name ? _data.name : "";

      if (_identifier != "") {
        // load the object via promise so we can send it back to the component
        // return promise
        return StaticService.getFeature(_identifier).then(
          (response) => {
            // We don't need to notify the user, just set it up
            const submitdata = {
              payload: response,
              identifier: _identifier,
              additionalName: _additionalName,
            };
            commit("setContent", submitdata);
            return Promise.resolve(response);
          },
          (error) => {
            _vm.$bvToast.toast(error.message, {
              title: "Error",
              autoHideDelay: 300,
              toaster: "b-toaster-bottom-right",
            });
          }
        );
      }
    },
  },
  mutations: {
    setLoading: (state, status) => {
      state.liloading = status;
    },
    setPresenting: (state, status) => {
      state.liPresenting = status;
    },
    setContent(state, data) {
      let name =
        data.additionalName != "" ? data.additionalName : data.identifier;
      state.staticContent = {
        ...state.staticContent,
        [name]: data.payload,
      };
    },
    setBuilderStatus: (state, status) => {
      state.liBuilder = status;
    },
    setBuilderSettingsWidgetItem: (state, status) => {
      state.liBuilderSettingsItem = status;
    },
  },
  getters: {
    getBuilderSettingsWidgetItem: (state) => {
      return state.liBuilderSettingsItem;
    },
    getBuilderStatus: (state) => {
      return state.liBuilder;
    },
    getLoading: (state) => {
      return state.liloading;
    },
    getPresenting: (state) => {
      return state.liPresenting;
    },
    getAppConfig: (state) => {
      return state.config;
    },
    getFeature: (state) => (name) => {
      // console.log(state.staticContent[name]);
      // console.log(();
      
      if (name in state.staticContent) {
        return state.staticContent[name];
      } else {
        
        return "error";
      }
    },
  },
};
