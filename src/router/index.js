import Vue from "vue";
import VueRouter from "vue-router";
import store from "../store";
// PUBLIC VIEWS
// AUTHENTICATION VIEWS
import Login from "../views/authentication/Login";
import Logout from "../views/authentication/Logout";
import Resetpassword from "../views/authentication/Resetpassword";
// END PUBLIC VIEWS
// PRIVATE VIEWS
// STATIC VIEWS
import Dashboard from "../views/static/Dashboard";
// DYNAMIC VIEWS
// import Features from '../views/dynamic/features';
import Feature from "../views/dynamic/Feature";
// import Feature from '../views/dynamic/features';

// END PRIVATE VIEWS

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "dashboard",
    component: Dashboard,
  },
  {
    path: "/login",
    name: "login",
    component: Login,
  },
  {
    path: "/logout",
    name: "logout",
    component: Logout,
  },
  {
    path: "/resetpassword",
    name: "resetpassword",
    component: Resetpassword,
  },
  
  {
    // PRESENTATIONS
    path: "/presentation/:identifier",
    name: "AppCourt - Presentation",
    component: () =>
      import(
        /* webpackChunkName: "presentation" */ "../views/dynamic/presentation.vue"
      ),
  },
  {
    // PRESENTATIONS
    path: "/presentation/:identifier/slide/:slide",
    name: "AppCourt - Presentation - Slide",
    component: () =>
      import(
        /* webpackChunkName: "presentation" */ "../views/dynamic/presentation.vue"
      ),
  },
  {
    // DATABASE
    path: "/database/:identifier",
    name: "AppCourt - Databases",
    component: () =>
      import(
        /* webpackChunkName: "presentation" */ "../views/dynamic/database.vue"
      ),
  },
  {
    // DATABASE
    path: "/database/:identifier/table/:slide",
    name: "AppCourt - Databases - Table",
    component: () =>
      import(
        /* webpackChunkName: "presentation" */ "../views/dynamic/database.vue"
      ),
  },
  {
    // SHARES
    path: "/share/:share?/:presentation?/:slide?",
    name: "shares",
    component: () =>
      import(
        /* webpackChunkName: "presentation" */ "../views/shares/share.vue"
      ),
  },
  {
    path: "/:identifier",
    name: "feature",
    component: Feature,
  },
  {
    path: "/:identifier/:item",
    name: "featuresedit",
    component: Feature,
  },
];

const router = new VueRouter({
  routes,
  linkActiveClass: "",
  linkExactActiveClass: "active",
  scrollBehavior(to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition;
    } else {
      return {
        x: 0,
        y: 0,
      };
    }
  },
});

router.beforeEach((to, from, next) => {
  store.state.liStatic.loading = true;
  const authRequired = to.name != "login" && to.name != "resetpassword"&& to.name != "shares";
  const loggedIn = localStorage.getItem("liSession");
  let appUser = null;
  let reconnectFlag = null;
  let authorizedPass = () => {
    // console.log('authorized');
    next();
  };
  let unAuthorizedPass = () => {
    // console.log('un-authorized');
    next("/login");
  };
  let getAppUser = new Promise((resolve) => {
    store
      .dispatch("auth/reconnect")
      .then((response) => {
        store.state.liStatic.loading = false;
        if (response.status == 400 || response.status == "error") {
          resolve("error");
        } else {
          resolve("good");
        }
      })
      .catch((error) => {
        resolve(error);
      });
  });

  // if(authRequired && storeUser === null) {
  if (authRequired) {
    reconnectFlag = true;
    getAppUser.then((response) => {
      if (response === "error" || response === "no session") {
        unAuthorizedPass();
      } else {
        authorizedPass();
      }
    });
  } else {
    // appUser = storeUser;
    if (authRequired && !loggedIn && !reconnectFlag && appUser != null) {
      store.state.liStatic.loading = false;
      // USER IS VIEWING AN PRIVATE PAGE, THERE IS NO TOKEN
      // console.log('not logged in');
      // next('/login');
      unAuthorizedPass();
    } else {
      // console.log('logged in follow through');
      // next();
      authorizedPass();
    }
  }
});

export default router;
