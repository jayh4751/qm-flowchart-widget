# flowchart-diagram documentation

## table of content

1. properties
2. Making new nodes
3. making new link-markers

---

## 2 Making new nodes (live mode and edit mode)

1. Go to src/JayhsWork/node.vue => 31 and add a svg with these properies:  
   ( v-if="node.shape === '.....'" //give the node a name  
    :x="x"  
    :y="y"  
    :style="svgNodeStyle(node)"  
    :height="" // 30 (standard)
   :width="" //130 (standard)
   )
2. Add a path depending if a path needs to be set with the property: d (make a node with svg path editor: https://yqnn.github.io/svg-path-editor/ or figma: )
3. add an if statement with the right returns (look at the other if statements for inspiration) in the function:

- textPosition() (for the text inside of the node)
- invisibleRecShapeByNode() ( for the deletebutton and the new link button)

4. Go to src/components/liBuilderDiagram.vue and change these functions (Edit mode):

- nodeAnchorPointsWithHeightAndWidth(node) ( positioning for: top/bottom/left/right of the node)
- arowDownAndTrashCanPosition(nodeX, nodeY, node) (positioning for the new link button and the delete button on the node)
- newLineFromNodeType(fromNodeId) (this will return the cordinates from where the newline will spawn from)

5. Go to src/components/live/liBuilderDiagramLive.vue and change only one function because editting is not available in live mode 4. (live mode)

- nodeAnchorPointsWithHeightAndWidth(node)

5. Go to src/JayhsWork/NodeSettings.vue -> 200 ( and add a value and text to shapes)

---

## 3 Making new link-markers

1. Go to src/components/liBuilderDiagram.vue and change these functions (edit mode)

- markerEnd(link)
- markerStart(link)

2. Go to src/components/liBuilderDiagram.vue and change the same function (live)

- markerEnd(link)
- markerStart(link)

2. Go to src/JayhsWork/LinkSettings.vue and change these things in data:

- IconEnd
- IconStart

---

<!-- https://stackoverflow.com/questions/43286666/fix-responsive-svg-breaking-out-of-bootstrap-grid -->
