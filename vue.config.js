const path = require('path');
const CKEditorWebpackPlugin = require('@ckeditor/ckeditor5-dev-webpack-plugin');
const { styles } = require('@ckeditor/ckeditor5-dev-utils');

// let CompressionPlugin = require("compression-webpack-plugin");       == ONLY WORKS IF WEBPACK 5 IS SUPPORTED BY VUE CLI 3.X
// const JavaScriptObfuscator = require('webpack-obfuscator');          == ONLY WORKS IF WEBPACK 5 IS SUPPORTED BY VUE CLI 3.X
// const encryption = true; // Whether the packaged code is encrypted   == ONLY WORKS IF WEBPACK 5 IS SUPPORTED BY VUE CLI 3.X


var mode = process.env.NODE_ENV || 'development';

console.log('vue config // mode = ', mode);

module.exports = {
    // NEW
    publicPath: '/', // // Basic path
    outputDir: 'dist', // A file name generated when packaging
    assetsDir: 'assets', // Static resource directory (js, css, img, fonts) these files can be written inside
    productionSourceMap: false, // Whether the production environment generates sourceMap files, it is generally not recommended to open
    // explicitly escape dependencies
    transpileDependencies: ['webpack-dev-server/client'],
    // chainWebpack: config => {
    //   // Specify entrance es6 to es5
    //   config.entry.app = ['babel-polyfill', './src/main.js'];
    // },
    runtimeCompiler: true,
    // END NEW
    parallel: false,
    // The source of CKEditor is encapsulated in ES6 modules. By default, the code
    // from the node_modules directory is not transpiled, so you must explicitly tell
    // the CLI tools to transpile JavaScript files in all ckeditor5-* modules.
    transpileDependencies: [
        /ckeditor5-[^/\\]+[/\\]src[/\\].+\.js$/,
    ],
    configureWebpack: config => {
        // devtool: false,
        if (mode == 'development') {
            return {
                plugins: [
                    // CKEditor needs its own plugin to be built using webpack.
                    new CKEditorWebpackPlugin({
                        // See https://ckeditor.com/docs/ckeditor5/latest/features/ui-language.html
                        language: 'en',

                        // Append translations to the file matching the `app` name.
                        translationsOutputFile: /app/
                    })
                ]
            }
        };
        if (mode === 'production') {
            return {
                plugins: [
                    // CKEditor needs its own plugin to be built using webpack.
                    new CKEditorWebpackPlugin({
                        // See https://ckeditor.com/docs/ckeditor5/latest/features/ui-language.html
                        language: 'en',

                        // Append translations to the file matching the `app` name.
                        translationsOutputFile: /app/
                    }),
                ],
            }
        };
        // if (mode === 'production' && encryption == true) {
        //     return {
        //         plugins: [
        //             // CKEditor needs its own plugin to be built using webpack.
        //             new CKEditorWebpackPlugin({
        //                 // See https://ckeditor.com/docs/ckeditor5/latest/features/ui-language.html
        //                 language: 'en',

        //                 // Append translations to the file matching the `app` name.
        //                 translationsOutputFile: /app/
        //             }),
        //             new CompressionPlugin({
        //                 algorithm: 'gzip', //'brotliCompress'
        //                 test: /\.js$|\.html$|\.css/, // + $|\.svg$|\.png$|\.jpg
        //                 threshold: 10240, // Compress data over 10k
        //                 deleteOriginalAssets: false // Do not delete the original file
        //             }),
        //             // js code encryption
        //             new JavaScriptObfuscator({
        //                 rotateUnicodeArray: true, // must be true
        //                 compact: true, // Compact Remove newline characters from the output obfuscation code.
        //                 controlFlowFlattening: false, // This option greatly affects the performance of a 1.5x speed reduction. Enable code control flow flattening. Control flow flattening is the structural transformation of source code, which hinders program understanding.
        //                 controlFlowFlatteningThreshold: 0.8,
        //                 deadCodeInjection: true, // This option greatly increases the size of obfuscated code (up to 200%) This feature adds random dead code blocks (ie: code that will not be executed) to the obfuscated output, making it more difficult to reverse engineer.
        //                 deadCodeInjectionThreshold: 0.5,
        //                 debugProtection: true, // Debugging protection If you open the developer tools, you can freeze your browser. 
        //                 debugProtectionInterval: true, // If checked, the interval forced debugging mode is used on the "Console" tab, which makes it more difficult to use other functions of "Developer Tools". How does it work? A special code that calls the debugger; inserted repeatedly throughout the obfuscated source code.
        //                 disableConsoleOutput: true, // Disable console.log, console.info, console.error and console.warn by replacing them with empty functions. This makes the use of the debugger more difficult.
        //                 domainLock: [], // Lock the obfuscated source code so that it only runs on specific domains and / or subdomains. This makes it very difficult for someone to simply copy and paste the source code and run it elsewhere. Multiple domains and subdomains can lock the code to multiple domains or subdomains. For example, to lock it so that the code only runs on www.example.com Add www.example.com so that it runs on any subdomain of example.com, use .example.com.
        //                 identifierNamesGenerator: 'hexadecimal', // Use this option to control the obfuscation of identifiers (variable names, function names, etc.).
        //                 identifiersPrefix: '', // This option makes all global identifiers have a specific prefix.
        //                 inputFileName: '',
        //                 log: false,
        //                 renameGlobals: false, // Don't start Enable global variable and function name confusion by declaration. 
        //                 reservedNames: [], // Disable obfuscation and generate identifiers that match the passed RegExp pattern. For example, if you add ^ someName, the obfuscator will ensure that all variables beginning with someName, the function name and function parameters will not be destroyed.
        //                 reservedStrings: [], // Disable the conversion of string literals, which match the RegExp pattern passed. For example, if you add ^ some * string, the obfuscator will ensure that all strings beginning with certain strings are not moved to `stringArray`.
        //                 rotateStringArray: true, // 
        //                 seed: 0, // By default (seed = 0), you will get a new result every time you obfuscate the code (ie: different variable names, different variables inserted into stringArray, etc.). If repeatable results are required, set the seed to a specific integer.
        //                 selfDefending: false, // This option makes the output code resistant to formatting and variable renaming. If you try to use a JavaScript beautifier on obfuscated code, the code will no longer work, making it more difficult to understand and modify it. Compact code setup is required.
        //                 sourceMap: false, // Make sure not to upload obfuscated source code with embedded source code, because it contains the original source code. Source mapping can help you debug obfuscated Java Script source code. If you want or need to debug in production, you can upload a separate source map file to a secret location and then point your browser to that location.
        //                 sourceMapBaseUrl: '', // This embeds the source mapping of the source into the result of obfuscated code. This is useful if you only want to debug locally on your computer.
        //                 sourceMapFileName: '',
        //                 sourceMapMode: 'separate',
        //                 stringArray: true, // Shift the stringArray array to a fixed and random (generated when code is confused) position. This makes it more difficult to match the order of deleted strings with their original positions. If the original source code is not small, it is recommended to use this option, because helper functions can attract attention.
        //                 stringArrayEncoding: false, // This option may slightly reduce the script speed. Use Base64 or RC4 to encode all string literals of stringArray and insert a special function to decode it back at runtime.
        //                 stringArrayThreshold: 0.8, // You can use this setting to adjust the probability that string literals will be inserted into stringArray (from 0 to 1). This setting is useful in large code bases, because repeated calls to the stringArray function will reduce the speed of the code.
        //                 target: 'browser', // You can set the target environment of the obfuscated code to one of the following: Browser, Browser No Eval, Node The current browser and node output are the same.
        //                 transformObjectKeys: false, // Convert (obfuscate) object keys. For example, this code var a = {enabled: true}; when using this option for obfuscation, the enabled object key will be hidden: var a = {}; a [_0x2ae0 [('0x0')] = true ;. Ideally used with String Array settings.
        //                 unicodeEscapeSequence: true, // Convert all strings to their unicode representation. For example, the string "Hello World!" Will be converted to "'\ x48 \ x65 \ x6c \ x6c \ x6f \ x20 \ x57 \ x6f \ x72 \ x6c \ x64 \ x21".
        //             }, ['abc.js']) // abc.js is not confusing code
        //         ],
        //     }
        // };

        // if (mode === 'production' && encryption == false) {
        //     return {
        //         plugins: [
        //              // CKEditor needs its own plugin to be built using webpack.
        //              new CKEditorWebpackPlugin({
        //                 // See https://ckeditor.com/docs/ckeditor5/latest/features/ui-language.html
        //                 language: 'en',

        //                 // Append translations to the file matching the `app` name.
        //                 translationsOutputFile: /app/
        //             }),
        //             new CompressionPlugin({
        //                 algorithm: 'gzip', //'brotliCompress'
        //                 test: /\.js$|\.html$|\.css/, // + $|\.svg$|\.png$|\.jpg
        //                 threshold: 10240, // Compress data over 10k
        //                 deleteOriginalAssets: false // Do not delete the original file
        //             }),
        //         ],
        //     }
        // };
                // //2019.8.30
        // Solved: [Vue warn]: You are using the runtime-only build of Vue where the template compiler is not available. Either pre-compile the templates into render functions, or use the compiler-included build. (Found in <Root >)
        config.resolve = {
            extensions: ['.js', '.vue', '.json', ".css"],
            alias: {
                'vue$': 'vue/dist/vue.esm.js'
            }
        };
    },

    // Vue CLI would normally use its own loader to load .svg and .css files, however:
    //	1. The icons used by CKEditor must be loaded using raw-loader,
    //	2. The CSS used by CKEditor must be transpiled using PostCSS to load properly.
    chainWebpack: config => {

        // Specify entrance es6 to es5
        // config.entry.app = ['babel-polyfill', './src/main.js'];
        // (1.) To handle editor icons, get the default rule for *.svg files first:
        const svgRule = config.module.rule('svg');

        // Then you can either:
        //
        // * clear all loaders for existing 'svg' rule:
        //
        //		svgRule.uses.clear();
        //
        // * or exclude ckeditor directory from node_modules:
        svgRule.exclude.add(path.join(__dirname, 'node_modules', '@ckeditor'));

        // Add an entry for *.svg files belonging to CKEditor. You can either:
        //
        // * modify the existing 'svg' rule:
        //
        //		svgRule.use( 'raw-loader' ).loader( 'raw-loader' );
        //
        // * or add a new one:
        config.module
            .rule('cke-svg')
            .test(/ckeditor5-[^/\\]+[/\\]theme[/\\]icons[/\\][^/\\]+\.svg$/)
            .use('raw-loader')
            .loader('raw-loader');

        // (2.) Transpile the .css files imported by the editor using PostCSS.
        // Make sure only the CSS belonging to ckeditor5-* packages is processed this way.
        config.module
            .rule('cke-css')
            .test(/ckeditor5-[^/\\]+[/\\].+\.css$/)
            .use('postcss-loader')
            .loader('postcss-loader')
            .tap(() => {
                return styles.getPostCssConfig({
                    themeImporter: {
                        themePath: require.resolve('@ckeditor/ckeditor5-theme-lark'),
                    },
                    minify: true
                });
            });
    }
};